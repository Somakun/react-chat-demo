import React, { Component } from 'react'
import '../style/inputWindow.css'

export default class InputWindow extends Component {

  idRand () {
    return Math.random().toString(36).substr(2, 9) + '-' + Math.random().toString(36).substr(2, 9);
  }

    state = {
      name: '',
      id: this.idRand(),
      text: '',
    }

  handleChange = event => {
    this.setState({
      text: event.target.value
    })
  }

  handleSubmit = event => {
    event.preventDefault()
    let obj = this.props.messages
    let newObj = obj.map(obj => obj )
    newObj.push(this.state)

    this.props.sendMessage(newObj)
    this.props.updateData(newObj)
    this.setState({
      text: ''
    })
    this.setState({id: this.idRand()})
  }

  render () {
    return (
      <div className="inputBar">
        <form onSubmit={this.handleSubmit}>
          <div className="sendBtn">
            <button type="submit">Send</button>
          </div>
          <div className="textInput">
            <label>
              <input className="input"
                     type="text"
                     name="text"
                     onChange={this.handleChange}
                     value={this.state.text}/>
            </label>
          </div>
        </form>
      </div>
    )
  }
}
