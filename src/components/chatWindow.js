import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import '../style/chatWindow.css'


class ChatWindow extends Component {

  scrollToBottom = () => {
    this.messagesEnd.scrollIntoView({ behavior: 'smooth' })
  }

  componentDidUpdate() {
    const node = ReactDOM.findDOMNode(this);
    this.shouldScrollBottom = node.scrollTop + node.clientHeight + 50 >= node.scrollHeight;
    if (this.shouldScrollBottom) {
      const node = ReactDOM.findDOMNode(this);
      node.scrollTop = node.scrollHeight
    }
    this.scrollToBottom()
  }


  render () {
    return (
      <div className="chatWindow">
        <ul>
          {this.props.messages.map(message =>
            <li className="message" key={message.id}>{message.text}</li>
          )}
        </ul>
        <div style={{ float:"left", clear: "both" }}
             ref={(el) => { this.messagesEnd = el; }}>
        </div>
      </div>
    )
  }
}

export default ChatWindow
