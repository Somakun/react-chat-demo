import React, { Component } from 'react'
import './App.css'
import ChatWindow from './components/chatWindow'
import InputWindow from './components/inputWindow'
import axios from 'axios'

class App extends Component {

  state = {
    messages: []
  };

  componentDidMount () {
    const baseUrl = `https://api.myjson.com/bins/w0yyo`

    axios.get(baseUrl)
      .then(res => {
        const messages = res.data
        this.setState({ messages })
      })
  }

  sendMessage (obj) {
    axios.put(`https://api.myjson.com/bins/w0yyo`, obj)
      .then(res => {
        console.log(res)
        console.log(res.data)
      })
  }

  updateData = (value) => {
    this.setState( {messages: value})
  }

  render () {
    return (
      <div className="App">
        <div className="container">
          <h1>Chat window demo</h1>
          <ChatWindow  messages={this.state.messages}/>
          <InputWindow messages={this.state.messages}
                       sendMessage={this.sendMessage}
                       updateData={this.updateData}/>
        </div>
      </div>
    )
  }
}

export default App
